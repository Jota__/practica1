﻿using System;
using System.Linq;

namespace Practica_1
{
    class Program
    {
        static void Main(string[] args)
        {
            ejercicio7();
        }

        public static void ejercicio1()

        {

            int contadoraprobados = 0;

            int contadorsobresalientes = 0;

            double nota;

            for (int i = 0; i < 10; i++)

            {
                Console.WriteLine("Inserte una nota");
                nota = Double.Parse(Console.ReadLine());
                if (nota >= 9)
                {
                    contadorsobresalientes++;
                }
                else if (nota >= 5)
                {
                    contadoraprobados++;
                }

            }
            Console.WriteLine("La cantidad de aprobados es " + contadoraprobados + " Y la cantidad de sobresalientes es " + contadorsobresalientes);

        }
        public static void ejercicio2()
        {
            Console.WriteLine("Inserte una fecha");

            DateTime date1 = DateTime.Parse(Console.ReadLine());

            Console.WriteLine("La fecha actual es: " + date1);

            date1 = date1.AddDays(90);

            Console.WriteLine("La fecha del pago es: " + date1);
        }

        public static void ejercicio3()
        {
            int i;

            int[] numeros = new int[10];

            numeros[0] = 0;

            numeros[1] = 1;

            numeros[2] = 7;

            numeros[3] = 9;

            numeros[4] = 12;

            numeros[5] = 18;

            numeros[6] = 89;

            numeros[7] = 43;

            numeros[8] = 23;

            numeros[9] = 78;
            for (i = 0; i < numeros.Length; i++)
            {
                Console.WriteLine(numeros[i]);
            }
            while (i > 0)
            {
                i--;
                Console.WriteLine(numeros[i]);
            }

        }
        public static void ejercicio5()

        {
            int n1, n2, n3, n4, n5, n6, n7, n8, n9, n10, mayor1, mayor2;
            Console.WriteLine("Introduzca el primer número");
            n1 = Int32.Parse(Console.ReadLine());
            Console.WriteLine("Introduzca el segundo número");
            n2 = Int32.Parse(Console.ReadLine());
            Console.WriteLine("Introduzca el tercer número");
            n3 = Int32.Parse(Console.ReadLine());
            Console.WriteLine("Introduzca el cuarto número");
            n4 = Int32.Parse(Console.ReadLine());
            Console.WriteLine("Introduzca el quinto número");
            n5 = Int32.Parse(Console.ReadLine());
            Console.WriteLine("Introduzca el sexto número");
            n6 = Int32.Parse(Console.ReadLine());
            Console.WriteLine("Introduzca el séptimo número");
            n7 = Int32.Parse(Console.ReadLine());
            Console.WriteLine("Introduzca el octavo número");
            n8 = Int32.Parse(Console.ReadLine());
            Console.WriteLine("Introduzca el noveno número");
            n9 = Int32.Parse(Console.ReadLine());
            Console.WriteLine("Introduzca el décimo número");
            n10 = Int32.Parse(Console.ReadLine());

            mayor1 = Math.Max(n1, Math.Max(n2, Math.Max(n3, Math.Max(n4, Math.Max(n5, Math.Max(n6, Math.Max(n7, Math.Max(n8, Math.Max(n9, n10)))))))));
            Console.WriteLine("El número mayor es: " + mayor1);
            if (mayor1 == n1)
            {
                mayor2 = Math.Max(n2, Math.Max(n3, Math.Max(n4, Math.Max(n5, Math.Max(n6, Math.Max(n7, Math.Max(n8, Math.Max(n9, n10))))))));
                Console.WriteLine("El segundo número mayor es: " + mayor2);
            }
            if (mayor1 == n2)
            {
                mayor2 = Math.Max(n1, Math.Max(n3, Math.Max(n4, Math.Max(n5, Math.Max(n6, Math.Max(n7, Math.Max(n8, Math.Max(n9, n10))))))));
                Console.WriteLine("El segundo número mayor es: " + mayor2);
            }
            if (mayor1 == n3)
            {
                mayor2 = Math.Max(n1, Math.Max(n2, Math.Max(n4, Math.Max(n5, Math.Max(n6, Math.Max(n7, Math.Max(n8, Math.Max(n9, n10))))))));
                Console.WriteLine("El segundo número mayor es: " + mayor2);
            }
            if (mayor1 == n4)
            {
                mayor2 = Math.Max(n1, Math.Max(n2, Math.Max(n3, Math.Max(n5, Math.Max(n6, Math.Max(n7, Math.Max(n8, Math.Max(n9, n10))))))));
                Console.WriteLine("El segundo número mayor es: " + mayor2);
            }
            if (mayor1 == n5)
            {
                mayor2 = Math.Max(n1, Math.Max(n2, Math.Max(n3, Math.Max(n4, Math.Max(n6, Math.Max(n7, Math.Max(n8, Math.Max(n9, n10))))))));
                Console.WriteLine("El segundo número mayor es: " + mayor2);
            }
            if (mayor1 == n6)
            {
                mayor2 = Math.Max(n1, Math.Max(n2, Math.Max(n3, Math.Max(n4, Math.Max(n5, Math.Max(n7, Math.Max(n8, Math.Max(n9, n10))))))));
                Console.WriteLine("El segundo número mayor es: " + mayor2);
            }
            if (mayor1 == n7)
            {
                mayor2 = Math.Max(n1, Math.Max(n2, Math.Max(n3, Math.Max(n4, Math.Max(n5, Math.Max(n6, Math.Max(n8, Math.Max(n9, n10))))))));

                Console.WriteLine("El segundo número mayor es: " + mayor2);
            }
            if (mayor1 == n8)

            {
                mayor2 = Math.Max(n1, Math.Max(n2, Math.Max(n3, Math.Max(n4, Math.Max(n5, Math.Max(n6, Math.Max(n7, Math.Max(n9, n10))))))));

                Console.WriteLine("El segundo número mayor es: " + mayor2);
            }
            if (mayor1 == n9)

            {
                mayor2 = Math.Max(n1, Math.Max(n2, Math.Max(n3, Math.Max(n4, Math.Max(n5, Math.Max(n6, Math.Max(n7, Math.Max(n8, n10))))))));

                Console.WriteLine("El segundo número mayor es: " + mayor2);
            }

            if (mayor1 == n10)

            {
                mayor2 = Math.Max(n1, Math.Max(n2, Math.Max(n3, Math.Max(n4, Math.Max(n5, Math.Max(n6, Math.Max(n7, Math.Max(n8, n9))))))));
                Console.WriteLine("El segundo número mayor es: " + mayor2);
            }
        }
        public static void ejercicio6()

        {
            int n1, n2, n3, n4, n5, n6, n7, n8, n9, n10, diferente;
            diferente = 10;
            Console.WriteLine("Introduzca el primer número");
            n1 = Int32.Parse(Console.ReadLine());
            Console.WriteLine("Introduzca el segundo número");
            n2 = Int32.Parse(Console.ReadLine());
            Console.WriteLine("Introduzca el tercer número");
            n3 = Int32.Parse(Console.ReadLine());
            Console.WriteLine("Introduzca el cuarto número");
            n4 = Int32.Parse(Console.ReadLine());
            Console.WriteLine("Introduzca el quinto número");
            n5 = Int32.Parse(Console.ReadLine());
            Console.WriteLine("Introduzca el sexto número");
            n6 = Int32.Parse(Console.ReadLine());
            Console.WriteLine("Introduzca el séptimo número");
            n7 = Int32.Parse(Console.ReadLine());
            Console.WriteLine("Introduzca el octavo número");
            n8 = Int32.Parse(Console.ReadLine());
            Console.WriteLine("Introduzca el noveno número");
            n9 = Int32.Parse(Console.ReadLine());
            Console.WriteLine("Introduzca el décimo número");
            n10 = Int32.Parse(Console.ReadLine());

            if (n1 == n2 || n1 == n3 || n1 == n4 || n1 == n5 || n1 == n6 || n1 == n7 || n1 == n8 || n1 == n9 || n1 == n10)
            {
                diferente--;
            }
            if (n2 == n1 | n2 == n3 | n2 == n4 | n2 == n5 | n2 == n6 | n2 == n7 | n2 == n8 | n2 == n9 | n2 == n10)
            {
                diferente--;
            }
            if (n3 == n2 | n1 == n3 | n3 == n4 | n3 == n5 | n3 == n6 | n3 == n7 | n3 == n8 | n3 == n9 | n3 == n10)
            {
                diferente--;
            }
            if (n4 == n2 | n4 == n3 | n1 == n4 | n4 == n5 | n4 == n6 | n4 == n7 | n4 == n8 | n4 == n9 | n4 == n10)
            {
                diferente--;
            }
            if (n5 == n2 | n5 == n3 | n5 == n4 | n1 == n5 | n5 == n6 | n5 == n7 | n5 == n8 | n5 == n9 | n5 == n10)
            {
                diferente--;
            }
            if (n6 == n2 | n6 == n3 | n6 == n4 | n6 == n5 | n1 == n6 | n6 == n7 | n6 == n8 | n6 == n9 | n6 == n10)
            {
                diferente--;
            }
            if (n7 == n2 | n7 == n3 | n7 == n4 | n7 == n5 | n7 == n6 | n1 == n7 | n7 == n8 | n7 == n9 | n7 == n10)
            {
                diferente--;
            }
            if (n8 == n2 | n8 == n3 | n8 == n4 | n8 == n5 | n8 == n6 | n8 == n7 | n1 == n8 | n8 == n9 | n8 == n10)
            {
                diferente--;
            }
            if (n9 == n2 | n9 == n3 | n9 == n4 | n9 == n5 | n9 == n6 | n9 == n7 | n9 == n8 | n1 == n9 | n9 == n10)
            {
                diferente--;
            }
            if (n10 == n2 | n10 == n3 | n10 == n4 | n10 == n5 | n10 == n6 | n10 == n7 | n10 == n8 | n10 == n9 | n1 == n10)
            {
                diferente--;
            }
            Console.WriteLine("Los valores diferentes son: " + diferente);
        }
        public static void ejercicio7()
        {
           
            int[] notas = new int[10];
            double media;
            for (int i = 0; i < notas.Length; i++)
            {
                Console.WriteLine("Inserte la nota " + (i + 1));
                notas[i] = int.Parse(Console.ReadLine());

            }
            if (notas.Max() - notas.Min() >= 3)
            {
                Console.WriteLine("La nota mayor: "+ notas.Max() +"  y la nota menor: "+ notas.Min() +" se desestimaron de la media");
                media = notas.Sum() - notas.Max();
                media = media - notas.Min();
                media = media / 8;
                Console.WriteLine("La media es " + media);
            }
            else
            {
                media = notas.Sum() / 10;
                Console.WriteLine("La media es " + media);
            }
        }

    }
}
